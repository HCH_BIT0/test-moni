# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class CreditRequest(models.Model):
    '''
    Representa todas las solicitudes de aprobacion de prestamo
    '''
    
    GENDER_CHOICES = (
        ('M', 'Masculino'),
        ('F', 'Femenino')
        )
    
    name = models.CharField('Nombre', max_length=80)
    surname = models.CharField('Apellido', max_length=80)
    id_number = models.CharField('Numero de documento', max_length=12)
    gender = models.CharField('Genero', choices=GENDER_CHOICES, max_length=1)
    email = models.EmailField('Email')
    approved = models.NullBooleanField('Aprobado', blank=True, null=True)
    error = models.NullBooleanField('Error', blank=True, null=True)
    
    def __unicode__(self):
        return u'{}, {}'.format(self.surname, self.name)