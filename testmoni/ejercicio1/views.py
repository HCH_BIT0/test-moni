# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http.response import JsonResponse
from django.views.generic.edit import FormView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.urls.base import reverse_lazy

import requests

from ejercicio1.forms import CreditRequestForm
from ejercicio1.models import CreditRequest
from django.contrib.auth.mixins import PermissionRequiredMixin


# Mixin para restringir acceso a miembros del staff
class AdminOnlyMixin(PermissionRequiredMixin):
    permission_required = 'is_admin'


# Vistas del panel de Administracion
class CreditRequestList(ListView, AdminOnlyMixin):
    model = CreditRequest


class CreditRequestUpdate(UpdateView, AdminOnlyMixin):
    model = CreditRequest
    success_url = reverse_lazy('cr-list')
    fields = ['name','surname', 'id_number', 'gender', 'email', 'approved', 'error']

    
class CreditRequestDelete(DeleteView, AdminOnlyMixin):
    model = CreditRequest
    success_url = reverse_lazy('cr-list')


# Vista del formulario de solicitud
class CreditRequestView(FormView):
    template_name = 'ejercicio1/creditrequest_form.html'
    form_class = CreditRequestForm
    success_url = '#'


# Funcion de consulta a endpoint
def doCreditRequest(request):
    if 'POST' in request.method and request.is_ajax():
        form = CreditRequestForm(request.POST)
        instance = form.save()
        id_number = instance.id_number
        gender = instance.gender
        email = instance.email
        url = 'http://scoringservice.moni.com.ar:7001/api/v1/scoring/?document_number={}&gender={}&email={}'
        url.format(id_number, gender, email)
        response = requests.get(url)
        if response.ok:
            r_json = response.json()
            instance.error = r_json['error']
            instance.approved = r_json['approved']
            instance.save()
            return JsonResponse(response.json())
        else:
            return JsonResponse(status=400)