'''
Created on 23 sept. 2017

@author: Hugo Chavero
'''
from django.forms.models import ModelForm
from ejercicio1.models import CreditRequest


class CreditRequestForm(ModelForm):
    class Meta:
        model = CreditRequest
        exclude = ['result', 'approved', 'error']
