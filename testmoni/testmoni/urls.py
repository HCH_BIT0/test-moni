from django.conf.urls import url
from ejercicio1.views import CreditRequestList, doCreditRequest,\
 CreditRequestView, CreditRequestUpdate, CreditRequestDelete


urlpatterns = [
    url(r'^admin/$', CreditRequestList.as_view(), name='cr-list'),
    url(r'^admin/edit/(?P<pk>\d+)$', CreditRequestUpdate.as_view(), name='cr-edit'),
    url(r'^admin/delete/(?P<pk>\d+)$', CreditRequestDelete.as_view(), name='cr-delete'),
    url(r'^docreditrequest/', doCreditRequest, name='credirrequest'),
    url(r'^', CreditRequestView.as_view(), name='credit-request-view'),
]
